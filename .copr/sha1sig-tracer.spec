Name:		sha1sig-tracer
Version:	{{ver}}
Release:	{{rel}}%{?dist}
Summary:	A tracing tool to aid testing Fedora's SHA-1 signature distrusting effort
License:	GPLv3+
URL:		https://gitlab.com/redhat-crypto/fedora-sha1sig-tracer
Source0:	https://gitlab.com/redhat-crypto/fedora-sha1sig-tracer/-/archive/{{git_commit}}/fedora-sha1sig-tracer-{{git_commit}}.tar.gz
BuildArch:	noarch

Requires:	python3-bcc
Conflicts:	openssl-libs < 3.0.2-4

%description
A tracing tool to aid testing Fedora's SHA-1 signature distrusting effort:
https://fedoraproject.org/wiki/Changes/StrongCryptoSettings3Forewarning1
It hooks into fedora-specific openssl USDT probes
and notifies the user of processes creating or verifying SHA-1 signatures,
as an alternative of switching to FUTURE crypto-policy and seeing what breaks.


%prep
%setup -q -n fedora-sha1sig-tracer-{{git_commit}}

%build
# noop

%install
install -d -m 755 %{buildroot}%{_sbindir}
install -m 775 sha1sig-tracer %{buildroot}%{_sbindir}/sha1sig-tracer

%files
%{_sbindir}/sha1sig-tracer

# templated from build.sh
%changelog
