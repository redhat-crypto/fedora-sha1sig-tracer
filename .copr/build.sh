#!/bin/bash
set -uexo pipefail

command -v git || dnf install -y git-core
command -v wget || dnf install -y wget

git_commit=$(git rev-parse HEAD)
version=$(git describe --tags --abbrev=0 | tail -c +2)
release=$(git describe --tags --long | tail -c +2 | sed s/^$version-// | sed s/-/./)

tarball=fedora-sha1sig-tracer-${git_commit}.tar.gz
mkdir -p ~/rpmbuild/SOURCES
[ -r ~/rpmbuild/SOURCES/$tarball ] || (
    wget https://gitlab.com/redhat-crypto/fedora-sha1sig-tracer/-/archive/$git_commit/fedora-sha1sig-tracer.tar.gz -O tmp
    mv tmp ~/rpmbuild/SOURCES/$tarball
)

cp $spec sha1sig-tracer.spec
sed -i "s|{{ver}}|$version|g" sha1sig-tracer.spec
sed -i "s|{{rel}}|$release|g" sha1sig-tracer.spec
sed -i "s|{{tarball}}|$tarball|g" sha1sig-tracer.spec
sed -i "s|{{git_commit}}|$git_commit|g" sha1sig-tracer.spec
git log "--format=* %cd %aN <%ae> - %h %n- %s%n" "--date=format:%a %b %d %Y" >> sha1sig-tracer.spec

rpmbuild -bs sha1sig-tracer.spec \
    --define "_srcrpmdir $outdir" \
    --define "_sourcedir $HOME/rpmbuild/SOURCES"

rm -f sha1sig-tracer.spec
