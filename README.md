# sha1sig-tracer

## Description

A tracing tool to aid testing Fedora's SHA-1 signature distrusting effort:
https://fedoraproject.org/wiki/Changes/StrongCryptoSettings3Forewarning1

It hooks into fedora-specific openssl USDT probes
and notifies the user of processes creating or verifying SHA-1 signatures,
as an alternative of switching to FUTURE crypto-policy and seeing what breaks.

## Installation from COPR

``` bash
# dnf copr enable asosedkin/sha1sig-tracer
# dnf install sha1sig-tracer
```

## Executing from a source checkout

``` bash
# dnf install python3-bcc
# ./sha1sig-tracer
```

## Example usage

Execute `sha1sig-tracer` as root, observe the output similar to:

```
# attaching...
libcrypto.so.3 probe attached: fedora_check_sig_level_1
libcrypto.so.3 probe attached: fedora_do_sigver_init_1
libcrypto.so.3 probe attached: fedora_evp_pkey_ctx_set_md_1
libcrypto.so.3 probe attached: fedora_ossl_digest_get_approved_nid_with_sha1_1
libcrypto.so.3 probe attached: fedora_ossl_digest_is_allowed_1
libcrypto.so.3 probe attached: fedora_ossl_digest_rsa_sign_get_md_nid_1
libcrypto.so.3 probe attached: fedora_ossl_digest_rsa_sign_get_md_nid_2
libssl.so.3 probe attached: fedora_ssl_security_cert_sig_1
libssl.so.3 probe attached: fedora_tls12_check_peer_sigalg_1
libssl.so.3 probe attached: fedora_tls12_sigalg_allowed_1
# tracing...
```

In a different shell:

``` bash
$ echo > infile
$ openssl genrsa -des3 -out key.pem
$ openssl dgst -sha1 -binary -out sha1 infile
$ openssl pkeyutl -inkey key.pem -sign -in sha1 -out sha1sig -pkeyopt digest:sha1
$ openssl pkeyutl -inkey key.pem -verify -sigfile sha1sig -in sha1 -pkeyopt digest:sha1
```

`sha1sig-tracer` should then print:

```
openssl[1536] uses mdnid=sha1 in fedora_ossl_digest_rsa_sign_get_md_nid_1
openssl[1537] uses mdnid=sha1 in fedora_ossl_digest_rsa_sign_get_md_nid_1
```

Terminate it with a `^C` to get a summary:

```
# summarizing...
`openssl` has triggered the following probes:
* fedora_ossl_digest_rsa_sign_get_md_nid_1 mdnid=sha1
# done
```
